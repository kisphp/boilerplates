## Installation

```shell
# Create virtual environment
python -m venv venv

# activate virtual env
source venv/bin/activate

# install dbt-postgres
pip install dbt-postgres

# (Optional) you can install multiple packages according to your needs
pip install \
dbt-core \
dbt-postgres \
dbt-redshift \
dbt-snowflake \
dbt-bigquery \
dbt-trino

# check dbt version
dbt --version # it should be at least version 1.7.3
```

## Start database 

```shell
# create docker containers
docker-compose up -d
```

This command will start two containers:
- postgres:16 available at 127.0.0.1:5432
- pgadmin4 available at http://localhost:8888

Connect to pgadmin4 and use the following creadentials to login:
- Email: admin@example.com
- Pass: admin

Then, you'll need to create the connection to the database:

1. click on **Add New Server** (it should be right in the middle of the screen)

![add-new-server-btn.png](add-new-server-btn.png)

2. A popup window will appear. Add a name for the server:

![img-1.png](img-1.png)

3. On the second tab **Connection** set the followings:

   - **Host name/address*: database (this is the name of the postgres database service from docker-compose.yml)
   - **Username**: kisphp_user
   - **Password**: kisphp_pass

![img-2.png](img-2.png)

Then click on **Save** button.

## Dummy data for playground

```sql
CREATE TABLE IF NOT EXISTS users (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  email VARCHAR(255),
  created_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS orders (
  id INT PRIMARY KEY,
  user_id INT,
  total_amount DECIMAL(10,2),
  order_status VARCHAR(255),
  created_at TIMESTAMP
);

CREATE TABLE IF NOT EXISTS order_items (
  id INT PRIMARY KEY,
  order_id INT,
  product_id INT,
  quantity INT,
  unit_price DECIMAL(10,2)
);

CREATE TABLE IF NOT EXISTS products (
  id INT PRIMARY KEY,
  name VARCHAR(255),
  price DECIMAL(10,2),
  description TEXT
);

INSERT INTO users VALUES ('1', 'John Doe', 'johndoe@example.com', '2023-12-04');
INSERT INTO users VALUES ('2', 'Jane Doe', 'janedoe@example.com', '2023-12-04');

INSERT INTO orders VALUES ('1', '1', '100.0', 'pending', '2023-12-04');
INSERT INTO orders VALUES ('2', '2', '50.0', 'completed', '2023-12-04');

INSERT INTO order_items VALUES ('1', '1', '1', '2', '50.0');
INSERT INTO order_items VALUES ('2', '1', '2', '1', '25.0');
INSERT INTO order_items VALUES ('3', '2', '3', '1', '50.0');

INSERT INTO products VALUES ('1', 'Product 1', '50.0', 'This is product 1.');
INSERT INTO products VALUES ('2', 'Product 2', '25.0', 'This is product 2.');
INSERT INTO products VALUES ('3', 'Product 3', '50.0', 'This is product 3.');
```

## DBT commands

```shell
# Run all seeds, models, snapshots, and tests in DAG order
dbt build

# Runs tests on data in deployed models.
dbt test

# List the resources in your project
dbt ls

# generate executable sql
dbt show --select my_first_dbt_model

# Compile SQL and execute against the current target...
dbt run
```
