data "aws_caller_identity" "current" {}
data "aws_availability_zones" "available" {}

locals {
    name            = "devops"
    cluster_version = "1.29"

    tags = module.tags.default_tags
}

module "eks" {
    source  = "terraform-aws-modules/eks/aws"
    version = "~> 20.0"

    cluster_name                   = local.name
    cluster_version                = local.cluster_version
    cluster_endpoint_public_access = true
    cluster_endpoint_private_access = true

    enable_cluster_creator_admin_permissions = true

    cluster_addons = {
        coredns = {
            most_recent = true
            resolve_conflicts = "OVERWRITE"
        }
        kube-proxy = {
            most_recent = true
            resolve_conflicts = "OVERWRITE"
        }
#        eks-pod-identity-agent = {
#            most_recent = true
#            resolve_conflicts = "OVERWRITE"
#        }
#        aws-efs-csi-driver = {
#            most_recent = true
#            resolve_conflicts = "OVERWRITE"
#        }
        vpc-cni = {
            most_recent    = true
            before_compute = true
            resolve_conflicts = "OVERWRITE"
            configuration_values = jsonencode({
                env = {
                    # Reference docs https://docs.aws.amazon.com/eks/latest/userguide/cni-increase-ip-addresses.html
                    ENABLE_PREFIX_DELEGATION = "true"
                    WARM_PREFIX_TARGET       = "1"
                }
            })
        }
    }

    vpc_id                   = data.aws_vpc.vpc.id
    subnet_ids               = data.aws_subnets.private.ids
    control_plane_subnet_ids = data.aws_subnets.intra.ids

    eks_managed_node_group_defaults = {
        ami_type       = "AL2_x86_64"
        instance_types = ["t3.small", "t3.medium"]
    }

    eks_managed_node_groups = {
        # Default node group - as provided by AWS EKS
        default_node_group = {
            use_custom_launch_template = false

            disk_size = 50

            min_size     = 1
            max_size     = 7
            desired_size = 3
        }
    }

    tags = local.tags
}

resource "aws_iam_policy" "node_additional" {
    name        = "${local.name}-additional"
    description = "Example usage of node additional policy"

    policy = jsonencode({
        Version = "2012-10-17"
        Statement = [
            {
                Action = [
                    "ec2:Describe*",
                ]
                Effect   = "Allow"
                Resource = "*"
            },
        ]
    })

    tags = local.tags
}

#module "lb_role" {
#    source    = "terraform-aws-modules/iam/aws//modules/iam-role-for-service-accounts-eks"
#
#    role_name = "aws-load-balancer-controller"
#    attach_load_balancer_controller_policy = true
#
#    oidc_providers = {
#        main = {
#            provider_arn = module.eks.oidc_provider_arn
#            namespace_service_accounts = ["kube-system:aws-load-balancer-controller"]
#        }
#    }
#}

#output "debug" {
#    value = module.eks
#}

#provider "kubernetes" {
#    host                   = module.eks.cluster_endpoint
#    cluster_ca_certificate = base64decode(module.eks.cluster_certificate_authority_data)
#
#    exec {
#        api_version = "client.authentication.k8s.io/v1beta1"
#        args        = ["eks", "get-token", "--cluster-name", local.name]
#        command     = "aws"
#    }
#}

#resource "helm_release" "secrets-store-csi-driver" {
#    name       = "secrets-store-csi-driver"
#    repository = "https://kubernetes-sigs.github.io/secrets-store-csi-driver/charts"
#    chart      = "secrets-store-csi-driver"
#    version    = "1.3.4"
#    namespace  = "kube-system"
#    timeout    = 10 * 60
#
#    values = [
#        <<VALUES
#    syncSecret:
#      enabled: true   # Install RBAC roles and bindings required for K8S Secrets syncing if true (default: false)
#VALUES
#    ]
#}

#data "kubectl_file_documents" "aws-secrets-manager" {
#    content = file("./aws-secrets-manager.yaml")
#}
## https://raw.githubusercontent.com/aws/secrets-store-csi-driver-provider-aws/main/deployment/aws-provider-installer.yaml
#
#resource "kubectl_manifest" "aws-secrets-manager" {
#    for_each  = data.kubectl_file_documents.aws-secrets-manager.manifests
#    yaml_body = each.value
#}

#resource "kubernetes_service_account" "service-account" {
#    metadata {
#        name      = "aws-load-balancer-controller"
#        namespace = "kube-system"
#        labels = {
#            "app.kubernetes.io/name"      = "aws-load-balancer-controller"
#            "app.kubernetes.io/component" = "controller"
#        }
#        annotations = {
#            "eks.amazonaws.com/role-arn"               = module.lb_role.iam_role_arn
#            "eks.amazonaws.com/sts-regional-endpoints" = "true"
#        }
#    }
#}
#
#resource "helm_release" "alb-controller" {
#    name       = "aws-load-balancer-controller"
#    repository = "https://aws.github.io/eks-charts"
#    chart      = "aws-load-balancer-controller"
#    namespace  = "kube-system"
#    depends_on = [
#        kubernetes_service_account.service-account
#    ]
#
#    set {
#        name  = "region"
#        value = local.region
#    }
#
#    set {
#        name  = "vpcId"
#        value = data.aws_vpc.vpc.id
#    }
#
#    set {
#        name  = "image.repository"
#        value = "602401143452.dkr.ecr.${local.region}.amazonaws.com/amazon/aws-load-balancer-controller"
#    }
#
#    set {
#        name  = "serviceAccount.create"
#        value = "false"
#    }
#
#    set {
#        name  = "serviceAccount.name"
#        value = "aws-load-balancer-controller"
#    }
#
#    set {
#        name  = "clusterName"
#        value = local.name
#    }
#}
