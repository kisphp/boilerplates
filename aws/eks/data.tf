data "aws_vpc" "vpc" {
  filter {
    name   = "tag:Name"
    values = [local.environment]
  }
}

data "aws_subnets" "public" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Name"
    values = ["${local.environment}-public-*"]
  }
}

data "aws_subnets" "private" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Name"
    values = ["${local.environment}-private-*"]
  }
}

data "aws_subnets" "intra" {
  filter {
    name   = "vpc-id"
    values = [data.aws_vpc.vpc.id]
  }

  filter {
    name   = "tag:Name"
    values = ["${local.environment}-intra-*"]
  }
}

data "aws_ami" "eks_default" {
    most_recent = true
    owners      = ["amazon"]

    filter {
        name   = "name"
        values = ["amazon-eks-node-${local.cluster_version}-v*"]
    }
}
