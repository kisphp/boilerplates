locals {
    repositories = [
        "kisphp-app-php",
        "kisphp-app-nginx",
    ]
}

module "ecr-repositories" {
    source = "terraform-aws-modules/ecr/aws"

    for_each = toset(local.repositories)

    repository_name = each.value

    repository_lifecycle_policy = jsonencode({
        rules = [
            {
                rulePriority = 1,
                description  = "Keep last 5 images",
                selection = {
                    tagStatus     = "tagged",
                    tagPrefixList = ["v"],
                    countType     = "imageCountMoreThan",
                    countNumber   = 5
                },
                action = {
                    type = "expire"
                }
            }
        ]
    })

    tags = module.tags.default_tags
}
