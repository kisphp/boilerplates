locals {
    name = "kisphp"
}

resource "aws_iam_user" "main" {
    name = local.name
    force_destroy = true
    path = "/"
}

resource "aws_iam_access_key" "main" {
    user = aws_iam_user.main.name
}

resource "aws_iam_user_policy_attachment" "ecr_access" {
    policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
    user       = aws_iam_user.main.name
}

output "AWS_ACCESS_KEY_ID" {
    value = aws_iam_access_key.main.id
}

output "AWS_SECRET_ACCESS_KEY" {
    value = aws_iam_access_key.main.secret
    sensitive = true
}

# terraform output -json | jq -r '.[].value'


# Secrets Manager
resource "aws_secretsmanager_secret" "iam_user" {
    name = "iam_user_${local.name}"
    # force immediate deletion
    recovery_window_in_days = 0
}

resource "aws_secretsmanager_secret_version" "iam_user_credentials" {
    secret_id     = aws_secretsmanager_secret.iam_user.id
    secret_string = jsonencode({
        AWS_ACCESS_KEY_ID: aws_iam_access_key.main.id,
        AWS_SECRET_ACCESS_KEY: aws_iam_access_key.main.secret,
    })
}

output "iam_user_secret_arn" {
    value = aws_secretsmanager_secret.iam_user.arn
}
