// TERRAMATE: GENERATED AUTOMATICALLY DO NOT EDIT

locals {
  environment = "devops"
  region      = "us-east-1"
}
provider "aws" {
  profile = "la"
  region  = local.region
  default_tags {
    tags = module.tags.default_tags
  }
}
module "tags" {
  code_source = "https://gitlab.com/kisphp/boilerplates.git/aws"
  environment = local.environment
  service     = "iam"
  source      = "../_modules/tags"
}
terraform {
  required_providers {
  }
}
