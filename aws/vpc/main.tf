data "aws_caller_identity" "current" {}
data "aws_availability_zones" "available" {}

locals {
    name     = "devops"
    vpc_cidr = "10.0.0.0/16"
    azs      = slice(data.aws_availability_zones.available.names, 0, 3)
}

module "vpc" {
    source  = "terraform-aws-modules/vpc/aws"
    version = "~> 5.0"

    name = local.name
    cidr = local.vpc_cidr

    azs             = local.azs
    private_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 4, k)]
    public_subnets  = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 48)]
    intra_subnets   = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 52)]
    database_subnets = [for k, v in local.azs : cidrsubnet(local.vpc_cidr, 8, k + 58)]

    create_database_subnet_group = true

    enable_nat_gateway     = true
    single_nat_gateway     = true
    enable_ipv6            = true
    create_egress_only_igw = true

    public_subnet_ipv6_prefixes                     = [0, 1, 2]
    public_subnet_assign_ipv6_address_on_creation   = true

    private_subnet_ipv6_prefixes                    = [3, 4, 5]
    private_subnet_assign_ipv6_address_on_creation  = true

    intra_subnet_ipv6_prefixes                      = [6, 7, 8]
    intra_subnet_assign_ipv6_address_on_creation    = true

    database_subnet_ipv6_prefixes                   = [9, 10, 11]
    database_subnet_assign_ipv6_address_on_creation = true

    public_subnet_tags = {
        "kubernetes.io/role/elb" = 1
    }

    private_subnet_tags = {
        "kubernetes.io/role/internal-elb" = 1
    }
}
