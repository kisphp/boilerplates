data "aws_availability_zones" "available" {}

locals {
    name   = "devops"
    azs      = slice(data.aws_availability_zones.available.names, 0, 3)
}


module "security_group" {
    source  = "terraform-aws-modules/security-group/aws"
    version = "~> 5.0"

    name        = local.name
    description = "Complete MySQL example security group"
    vpc_id      = data.aws_vpc.vpc.id

    # ingress
    ingress_with_cidr_blocks = [
        {
            from_port   = 3306
            to_port     = 3306
            protocol    = "tcp"
            description = "MySQL access from within VPC"
            cidr_blocks = data.aws_vpc.vpc.cidr_block
        },
    ]
}


module "db" {
    source = "terraform-aws-modules/rds/aws"

    identifier = local.name

    engine               = "mysql"
    engine_version       = "8.0"
    family               = "mysql8.0" # DB parameter group
    major_engine_version = "8.0"      # DB option group
    instance_class       = "db.t3.medium"

    allocated_storage     = 20
    max_allocated_storage = 100

    db_name  = "kisphp_db"
    username = "kisphp_user"
    port     = 3306

    multi_az               = true
#    db_subnet_group_name   = module.vpc.database_subnet_group
#    db_subnet_group_name   = data.aws_subnets.db.ids
    db_subnet_group_name   = data.aws_db_subnet_group.db.name
    vpc_security_group_ids = [module.security_group.security_group_id]

    maintenance_window              = "Mon:00:00-Mon:03:00"
    backup_window                   = "03:00-06:00"
    enabled_cloudwatch_logs_exports = ["general"]
    create_cloudwatch_log_group     = true

    skip_final_snapshot = true
    deletion_protection = false

    performance_insights_enabled          = true
    performance_insights_retention_period = 7
    create_monitoring_role                = true
    monitoring_interval                   = 60

    parameters = [
        {
            name  = "character_set_client"
            value = "utf8mb4"
        },
        {
            name  = "character_set_server"
            value = "utf8mb4"
        }
    ]

    db_instance_tags = {
        "Sensitive" = "high"
    }
    db_option_group_tags = {
        "Sensitive" = "low"
    }
    db_parameter_group_tags = {
        "Sensitive" = "low"
    }
    db_subnet_group_tags = {
        "Sensitive" = "high"
    }
}

# module.vpc.database_subnet_group
data "aws_db_subnet_group" "db" {
    name = local.name
}

output "db_instance_address" {
    value = module.db.db_instance_address
}
output "db_instance_master_user_secret_arn" {
    value = module.db.db_instance_master_user_secret_arn
}
output "db_instance_name" {
    value = module.db.db_instance_name
}
output "db_instance_port" {
    value = module.db.db_instance_port
}
output "db_instance_username" {
    value = module.db.db_instance_username
    sensitive = true
}
