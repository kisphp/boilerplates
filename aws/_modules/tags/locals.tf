locals {
    source_path = format(
        "%s%s",
        var.code_source, replace(
            abspath(path.root),
            dirname(abspath(path.cwd)),
            ""
        )
    )

    compiled_tags = merge(
        {
            service     = var.service
            environment = var.environment
            managed-by  = var.managed_by
            source      = coalesce(var.code_source_override, local.source_path)
        },
        var.cost_center == "" ? {} : { cost-center = var.cost_center },
        var.sub_cost_center == "" ? {} : { sub-cost-center = var.sub_cost_center },
        var.subsystem == "" ? {} : { sub-system = var.subsystem },
        var.ticket == "" ? {} : { ticket = var.ticket },
        var.owner_email == "" ? {} : { owner-email = var.owner_email },
        var.docu_link == "" ? {} : { documentation-link = var.docu_link },
        var.product == "" ? {} : { product = var.product },
        var.project == "" ? {} : { project = var.project },
        var.dependent_on == "" ? {} : { depends-on = var.dependent_on },
        var.deprecate_after == "" ? {} : { deprecate-after = var.deprecate_after },
        var.terminate_after == "" ? {} : { terminate-after = var.terminate_after },
        var.additional_tags
    )

    tags_list = [ for k, v in local.compiled_tags : "${k}=${v}" ]
}

output "default_tags" {
    value = local.compiled_tags
}

output "default_tags_list" {
    value = local.tags_list
}
