variable "environment" {
    type        = string
    description = "(Required) Environment name"

    validation {
        condition     = contains(["all", "development", "staging", "test", "devops", "production"], var.environment)
        error_message = "Environment must be one of: all, development, staging, test, devops, production"
    }
}

variable "service" {
    type        = string
    description = "(Required) Service name"
}

variable "code_source" {
    type        = string
    description = "(Required) A reference to the repository in which this resource is set"

    #  Examples:
    #    gh:ubuntu/terraform-modules
    #    gh:ubuntu/infrastructure-one
    #    gh:ubuntu/infrastructure-two
}

variable "code_source_override" {
    type        = string
    description = "(Optional) Override the Source tag value"
    default     = ""
}

variable "additional_tags" {
    type        = map(string)
    description = "(Optional) Add other tags that were not covered by the default use cases. Dynamic tags can overwrite pre configured tags."
    default     = {}

    # Example
    #  additional_tags = {
    #    team = "devops"
    #    subteam = "security"
    #  }
}

variable "cost_center" {
    type        = string
    default     = ""
    description = "(Optional) Cost center keyword"
}

variable "sub_cost_center" {
    type        = string
    default     = ""
    description = "(Optional) Sub cost center keyword"
}

variable "project" {
    type        = string
    default     = ""
    description = "(Optional) A name or a reference to the project this entity belongs to"
}

variable "product" {
    type        = string
    default     = ""
    description = "(Optional) A name or a reference to the product this entity belongs to"
}

variable "subsystem" {
    type        = string
    default     = ""
    description = "(Optional) A name of subsystem this entity is part of"
}

variable "dependent_on" {
    type        = string
    default     = ""
    description = "(Optional) A reference to a service/system/subsystem this entity (in)directly depends on"
}

variable "owner_email" {
    type        = string
    default     = ""
    description = "(Optional) An email address to a sole owner of this entity"

    validation {
        condition     = var.owner_email == "" || can(regex("^[^\\s@]+@[^\\s@]+\\.[^\\s@]{2,}$", var.owner_email))
        error_message = "Owner must be a valid email address"
    }
}

variable "ticket" {
    type        = string
    default     = ""
    description = "(Optional) A reference to ticket describing a purpose of this entity"
}

variable "docu_link" {
    type        = string
    default     = ""
    description = "(Optional) A link to a documentation referring or requiring this entity to exist"
}

variable "deprecate_after" {
    type        = string
    default     = ""
    description = "(Optional) A timestamp in the future marking the deprecation point for this entity"
}

variable "terminate_after" {
    type        = string
    default     = ""
    description = "(Optional) A timestamp in the future after which existence of the entity is meaningless"
}

variable "managed_by" {
    type        = string
    default     = "terraform"
    description = "(Optional) Name of the management tool for the resource"
}
