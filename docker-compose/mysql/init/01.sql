use kisphp_db;

CREATE TABLE t1 (
    d_id INT,
    a_id INT,
    l_id INT
);

INSERT INTO t1 SET d_id = 1, a_id = 1, l_id = 1;
INSERT INTO t1 SET d_id = 2, a_id = 2, l_id = 2;
INSERT INTO t1 SET d_id = 3, a_id = 3, l_id = 3;
INSERT INTO t1 SET d_id = 4, a_id = 4, l_id = 4;

CREATE TABLE t2 (
    l_id INT,
    licence_code INT
);
INSERT INTO t2 SET l_id = 1, licence_code = 2;
INSERT INTO t2 SET l_id = 2, licence_code = 3;
INSERT INTO t2 SET l_id = 3, licence_code = 4;

CREATE TABLE t3 (
    d_id INT,
    licence_code INT
);

INSERT INTO t3 SET d_id = 3, licence_code = 3;
INSERT INTO t3 SET d_id = 4, licence_code = 4;
INSERT INTO t3 SET d_id = 5, licence_code = 5;
INSERT INTO t3 SET d_id = 6, licence_code = 6;
