SELECT re.id, re.customer_id, re.message -> 'message', c.first_name, c.last_name
FROM reservation_enquires AS re
LEFT JOIN customer AS c ON (c.id = re.customer_id)
