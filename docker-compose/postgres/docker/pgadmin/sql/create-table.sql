CREATE TABLE reservation_enquires(
    id serial PRIMARY KEY,
    customer_id int,
    message jsonb
);
CREATE TABLE merchant_customer(
    id serial PRIMARY KEY,
    customer_id int
);
CREATE TABLE customer(
    id serial PRIMARY KEY,
    first_name varchar(64),
    last_name varchar(64)
);