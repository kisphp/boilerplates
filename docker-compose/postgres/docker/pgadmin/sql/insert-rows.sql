INSERT INTO reservation_enquires (id, customer_id, message)
VALUES
(1, 1, '{"message": "text 1"}'),
(2, 3, '{"message": "text 2"}');

INSERT INTO customer (id, first_name, last_name)
VALUES (1, 'John', 'Smith'), (2, 'Jack', 'Sparrow');

INSERT INTO merchant_customer (id, customer_id)
VALUES (2, 3);
