module "tags" {
  source = "git::git@gitlab.com:kisphp-tfmods/tags.git"

  repo_url = "git@gitlab.com:kisphp/bolerplates.git"
  repo_path = split("terraform/start", path.cwd)[1]

  service = "demo"
  name = "s3-bucket"
}
