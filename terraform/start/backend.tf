terraform {
  required_version = "~> 1.5.7"

  backend "s3" {
    bucket         = "{s3-bucket-name}"
    key            = "{project-name}/terraform.tfstate"
    region         = "{aws-region}"
    dynamodb_table = "terraform-state-lock"
    encrypt        = true
  }

  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 5.0"
    }
  }
}
