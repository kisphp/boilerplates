provider "aws" {
  region              = "eu-central-1"
  allowed_account_ids = ["{aws-account-id}"]
#  profile = "demo"

  default_tags {
    tags = module.tags.default_tags
  }
}
